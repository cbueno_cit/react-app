import './App.css';

import React from 'react';
import logo from './logo.svg';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title: process.env.REACT_APP_TITLE,
      data: []
    }
  }

  async componentDidMount() {
    const base_api = process.env.REACT_APP_BASE_API;
    const response = await fetch(`${base_api}/discover/movie?api_key=c0ae994ab1d00d353f72d6ac8303a891`, {
      method: 'GET',
      headers: { 
        'Content-Type': 'application/json'
      }
    });

    const data = await response.json();

    this.setState({
      data: data.results
    });
  }

  render() {          
    return (
      <div className="App">
        <header className="App-header">
          <h1>{this.state.title}</h1>
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Movies</h1>
          {
            this.state.data.map(item => {
              return <Movie title={item.title} relase_date={item.release_date}/>
            })
          }
        </header>        
      </div>
    );
  }
}

class Movie extends React.Component {
  render() {
    return (
      <div>
        <p>{this.props.title}</p>
        <p>{this.props.relase_date}</p>
      </div>
    );
  }
}

export default App;
